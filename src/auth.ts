import React from 'react';
import { useState, useEffect, createContext, ReactNode, useContext } from 'react';

interface Auth {
    loggedIn: boolean;
}

interface AuthContextProps {
    auth: Auth;
    login: () => void;
    logout: () => void;
}

// initial state
const initialAuth: Auth = {
    loggedIn: false,
};

// create context
export const AuthContext = createContext<AuthContextProps>(undefined);

export const AuthProvider: React.FC<{ children: ReactNode }> = ({ children }) => {
    // Your authentication logic goe here
    const [auth, setAuth] = React.useState<Auth>({ loggedIn: false });
  
    const login = () => {
      setAuth({ loggedIn: true });
    };
  
    const logout = () => {
      setAuth({ loggedIn: false });
    };
  
    const contextValue = {
      auth,
      login,
      logout,
    };
  
    // Je comprends pas pourquoi ca marche pas
    return '';
    // return <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>;
  };

  
export const useAuth = () => {
    const context = useContext(AuthContext);
    if (!context) {
        throw new Error('useAuth must be used within an AuthProvider');
    }
    return context;
};

export default AuthContext;
