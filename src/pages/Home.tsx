import MessageListItem from '../components/MessageListItem';
import { PinListItem, NewPinCard } from '../components/PinListItem';
import { useState } from 'react';
import { Message, getMessages } from '../data/messages';
import { Pin, getPins } from '../data/pins';
import {
  IonContent,
  IonHeader,
  IonList,
  IonPage,
  IonRefresher,
  IonRefresherContent,
  IonTitle,
  IonToolbar,
  useIonViewWillEnter
} from '@ionic/react';
import './Home.css';

const Home: React.FC = () => {

  const [pins, setPins] = useState<Pin[]>([]);

  useIonViewWillEnter(() => {
    const pins = getPins();
    setPins(pins);
  });

  const refresh = (e: CustomEvent) => {
    setTimeout(() => {
      e.detail.complete();
    }, 3000);
  };

  return (
    <IonPage id="home-page">
      <IonHeader>
        <IonToolbar>
          <IonTitle>Inbox</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonRefresher slot="fixed" onIonRefresh={refresh}>
          <IonRefresherContent></IonRefresherContent>
        </IonRefresher>

        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">
              Inbox
            </IonTitle>
          </IonToolbar>
        </IonHeader>

        <IonList>
          <NewPinCard onSave={(newPin) => {
            setPins([newPin, ...pins]);
          }
          } />
          {pins.map(p => <PinListItem key={p.id} pin={p} />)}
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Home;
