export interface Pin {
  title: string;
  text: string;
  id: number;
}

const fakeText = "lore ipsum dollore ipsum dolor sit amet dolor sit amet dolor sit amet lorem ipsum dolor sit amet dolor sit amet dolor sit ametlore ipsum dolor sit amet dolor sit amet dolor sit amet lorem ipsum dolor sit amet dolor sit amet dolor sit ametlore ipsum dolor sit amet dolor sit amet dolor sit amet ";

const pins: Pin[] = [

  
  {
    "title": "Pin 1",
    "text": fakeText,
    "id": 1
  },
  {
    "title": "Pin 2",
    "text": fakeText,
    "id": 2
  },

];

export const getPins = () => pins;

export const getPin = (id: number) => pins.find(p => p.id === id);
