import {
  IonCard,
  IonCardContent,
  IonInput,
  IonItem,
  IonLabel,
  IonButton,
  IonCardTitle,
  IonIcon,
} from '@ionic/react';
import { Pin, getPins } from '../data/pins';
import './PinListItem.css';
import { useState } from 'react';
import { addOutline } from 'ionicons/icons';

interface PinListItemProps {
  pin: Pin;
}

const PinListItem: React.FC<PinListItemProps> = ({ pin }) => {
  // Truncate the text if it exceeds the maximum length
  const truncatedText =
    pin.text.length > 50
      ? `${pin.text.slice(0, 50)}...`
      : pin.text;

  return (
    <IonItem routerLink={`/pin/${pin.id}`} detail={false}>
      <div slot="start" className="dot dot-unread"></div>
      <IonLabel className="ion-text-wrap">
        <h2>
          {pin.title}
        </h2>

        <span>{truncatedText}</span>
      </IonLabel>
    </IonItem>
  );
};

const NewPinCard: React.FC<{
  onSave: (newPin: Pin) => void;
}> = ({ onSave }) => {
  const [newPinTitle, setNewPinTitle] = useState('');
  const [newPinText, setNewPinText] = useState('');

  const handleSave = () => {
    const newPin: Pin = {
      id: getPins().length + 1,
      title: newPinTitle,
      text: newPinText,
    };

    onSave(newPin);

    // add the new pin to the list of pins
    // TEMPORAIRE car pas de base de données et pas de persistence des données
    getPins().push(newPin);

    setNewPinTitle('');
    setNewPinText('');
  };

  return (
    <IonCard>
      <IonCardContent>
        <IonCardTitle>Créer une nouvelle épingle</IonCardTitle>
        <IonLabel>Titre :</IonLabel>
        <IonInput
          value={newPinTitle}
          onIonChange={(e) => setNewPinTitle(e.detail.value!)}
        ></IonInput>
        <IonLabel>Citation :</IonLabel>
        <IonInput
          placeholder="Ex : La vérité est une et indivisible"
          value={newPinText}
          onIonChange={(e) => setNewPinText(e.detail.value!)}
        ></IonInput>
        <IonButton fill="clear" onClick={handleSave}>
        <IonIcon icon={addOutline} style={{ marginRight: '5px' }} />
          Créer
        </IonButton>
      </IonCardContent>
    </IonCard>
  );
};

export { PinListItem, NewPinCard };